import connection.DvDConsumer;

public class App {

    public static void main(String[] args) {

        DvDConsumer consumer = new DvDConsumer("messageQueue");
        consumer.addSubscriber("ncoman32@yahoo.com");
        consumer.addSubscriber("diad995@gmail.com");
        consumer.start();

    }
}
