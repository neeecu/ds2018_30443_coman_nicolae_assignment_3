package connection;

import business.DiskWriteService;
import business.MailService;
import com.google.gson.Gson;
import com.rabbitmq.client.*;
import model.DvD;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class DvDConsumer extends Thread implements Consumer {

    private Channel channel;
    private Connection connection;
    private String queueName;
    private ConnectionFactory factory;
    private MailService mailService;
    private DiskWriteService diskWriteService;
    private List<String> receivers;

    public DvDConsumer(String queueName) {
        this.queueName = queueName;
        this.mailService = new MailService("mockingemailaddress@yahoo.com", "!abcd1234");
        this.factory = new ConnectionFactory();
        this.diskWriteService = new DiskWriteService();
        this.receivers = new ArrayList<String>();
        factory.setHost("localhost");

        try {
            this.connection = factory.newConnection();
            this.channel = connection.createChannel();
            // queueDeclare(queueName, durable?, exclusive?,autoDelete?,arguments)
            this.channel.queueDeclare(queueName, false, false, false, null);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public void addSubscriber(String subscriber) {
        this.receivers.add(subscriber);
    }

    public void run() {
        try {
            //start consuming messages. Auto acknowledge messages.
            channel.basicConsume(queueName, true, this);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    public void handleConsumeOk(String s) {
        System.out.println("Consumer " + s + " has subscribed");

    }

    public void handleCancelOk(String s) {

    }

    public void handleCancel(String s) throws IOException {

    }

    public void handleShutdownSignal(String s, ShutdownSignalException e) {

    }

    public void handleRecoverOk(String s) {

    }

    public void handleDelivery(String s, Envelope envelope, AMQP.BasicProperties basicProperties, byte[] bytes) throws IOException {
        String receivedString = new String(bytes, "UTF-8");
        String json = receivedString.split("\\+")[0];
        String className = receivedString.split("\\+")[1];

        Gson gson = new Gson();
        DvD receivedDvd;
        try {

            receivedDvd = (DvD) gson.fromJson(json, Class.forName(className));
            diskWriteService.writeToDisk(receivedDvd.getName(), json);

            for(String receiver: receivers)
                mailService.sendMail(receiver,"Consumed DvD",json);


        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }


    }
}
