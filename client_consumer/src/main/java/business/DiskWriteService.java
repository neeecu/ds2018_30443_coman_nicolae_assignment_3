package business;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class DiskWriteService {

    public void writeToDisk(String fileName, String content) throws IOException {
        String filePath = "/home/nicu/workspace/assignment_3/client_consumer/src/main/resources/"
                + fileName + ".txt";
        File f = new File(filePath);
        //if file exists, append text to it
        if (f.isFile()) {
            FileWriter writer = new FileWriter(f);
            writer.write("\n" + content);
            writer.flush();
            writer.close();
        } else {
            //the file must be created
            f.createNewFile();
            FileWriter writer = new FileWriter(f);
            writer.write(content);
            writer.flush();
            writer.close();
        }
    }
}
