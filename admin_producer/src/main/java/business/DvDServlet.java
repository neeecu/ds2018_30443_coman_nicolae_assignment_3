package business;

import connection.DvDProducer;
import model.DvD;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

public class DvDServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        RequestDispatcher requestDispatcher = req.getRequestDispatcher("dvd.html");
        requestDispatcher.forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        DvDProducer producer = new DvDProducer("messageQueue");

        resp.setContentType(HtmlStrings.HTML);
        PrintWriter out = resp.getWriter();

        String dvdName = req.getParameter("name");
        String year = req.getParameter("year");
        String price = req.getParameter("price");

        DvD toSend = new DvD(dvdName, Integer.parseInt(year), Double.parseDouble(price));
        producer.publish(toSend, toSend.getClass().getName());

        /*html doc start*/
        out.print(HtmlStrings.DOCTYPE);
        out.print(HtmlStrings.makeHead(" Published"));
        out.print(HtmlStrings.BODY_START_TAG);
        out.print(HtmlStrings.makeH("Name: " + dvdName, 3));
        out.print(HtmlStrings.makeH("Year: " + year, 3));
        out.print(HtmlStrings.makeH("Price: " + price, 3));
        out.print(HtmlStrings.BODY_END_TAG);
        out.print(HtmlStrings.END_HTML_TAG);
        /*html doc end*/

        out.print(HtmlStrings.makeButtonWithLink("http://localhost:8090/admin_producer_war/dvd.html", "Back"));


    }
}
