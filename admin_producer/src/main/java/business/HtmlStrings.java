package business;

import java.util.List;

public class HtmlStrings {
    public static final String DOCTYPE = "<!DOCTYPE html>\n" + "<html lang=\"en\">";
    public static final String END_HTML_TAG = "</html>";
    public static final String BODY_END_TAG = "</body>";
    public static final String BODY_START_TAG ="<body>";
    public static final String DIV_START_TAG = "<div>";
    public static final String DIV_END_TAG="</div>";
    public static final String HTML_NEW_LINE="<br></br>";
    public static final String HTML ="text/html";
    public static final String FORM_END_TAG="</form>";
    public static final String LIST_START_TAG=" <ui style=\"list-style: none;\">";
    public static final String LIST_END_TAG ="</ul>";
    public static final String LIST_ELEM_START_TAG ="<li style=\"padding :5px;\">";
    public static final String LIST_ELEM_END_TAG = "</li>";
    public static final String TABLE_END_TAG = "</table>";
    public static final String TABLE_PAGE_HEADER ="<head>\n" +
            "<style>\n" +
            "table, th, td {\n" +
            "    border: 1px solid black;\n" +
            "}\n" +
            "</style>\n" +
            "</head>";

    /*must be used inside a ul list*/
    public static String makeTextInputEntry(String placeholder,String name){
        return "<li style=\"padding :5px;\">  <input type=\"text\" " +
                "placeholder=\""+placeholder+"\" name=\""+name+"\"> </li>";
    }

    public static String makeTimeInputEntry(String name){
        return   "<li style=\"padding :5px;\"> <input type=\"datetime-local\" name=\""+name+"\"></li>";
    }

    public static String makeButtonInForm(String name, String value){
        return "<li style=\"padding :5px;\">  <input type=\"submit\" " +
                "value=\""+value+"\" name=\""+name+"\">  </li>";
    }

    /* message = message to be writte, number = type of h ; h1/h2/h3*/
    public static String makeH(String message, int number){
        return "<h"+String.valueOf(number)+">"+message+"</h"+String.valueOf(number)+">";
    }

    public static  String startForm(String action, String method){
        return "<form action=\"/"+action+"\"  method=\""+method+"\" >";
    }

    public static String makeHead(String title){
        return "<head>\n" +
                "    <meta charset=\"UTF-8\">\n" +
                "    <title>"+title+"</title>\n" +
                "</head>";
    }

    public static String makeParagraph(String text){
        return "<p>"+text+"</p>";
    }

    public static String makeButtonWithLink(String link, String buttonName){
        return "<a class=\"btn\" href=\""+link+"\">"+buttonName+"</a>";
    }

    public static String makeLabel(String text){
        return "<label>"+text+"</label>";
    }

    public static String makeDropDownList(List<String> values,String name) {
        StringBuilder dropDownList = new StringBuilder("");
        dropDownList.append("<select name=\"" + name + "\">");
        for(int i=0; i < values.size();i++){
            dropDownList.append("<option value=\""+values.get(i)+"\">"+values.get(i)+"</option>");
        }
        dropDownList.append("</select>");
        return dropDownList.toString();

    }

    public static String makeTableHeader(List<String> tableHeaderElements){
        StringBuilder table = new StringBuilder("");
        table.append("<table style=\"width:100%\">\n"  + "<tr>");
        for(int i=0;i<tableHeaderElements.size();i++){
            table.append("<th>"+tableHeaderElements.get(i)+"</th>\n");
        }
        table.append("</tr>");
        return table.toString();
    }

    public static String makeTableEntry(List<String> tableData){
        StringBuilder tableEntry = new StringBuilder("");
        tableEntry.append("<tr>");
        for(int i=0;i<tableData.size();i++){
            tableEntry.append("<td>"+tableData.get(i)+"</td>\n");
        }
        tableEntry.append("</tr>");
        return tableEntry.toString();
    }

}
