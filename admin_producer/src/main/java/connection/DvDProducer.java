package connection;

import com.google.gson.Gson;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;

import java.io.IOException;

public class DvDProducer {

    private String queueName;
    private Channel channel;
    private ConnectionFactory factory;
    private Connection connection;

    public DvDProducer(String queueName) {
        this.queueName = queueName;
    }


    private void openConnection() {
        this.factory = new ConnectionFactory();
        factory.setHost("localhost");

        try {
            this.connection = factory.newConnection();
        } catch (IOException e) {
            e.printStackTrace();
        }

        try {
            this.channel = connection.createChannel();
            // queueDeclare(queueName, durable?, exclusive?,autoDelete?,arguments)
            this.channel.queueDeclare(queueName, false, false, false, null);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void closeConnection() {
        try {
            channel.close();
            connection.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void publish(Object o, String className) {

        openConnection();
        Gson gson = new Gson();
        String objectString = gson.toJson(o);
        String toSend = objectString + "+" + className;
        try {
            channel.basicPublish("", queueName, null, toSend.getBytes());
        } catch (IOException e) {
            e.printStackTrace();
        }
        closeConnection();
    }

}
